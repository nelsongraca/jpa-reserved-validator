# README #

## Why? ##
When developing applications where there is the need to support multiple DBMS JPA may help a lot,
but then there is the need to be careful about the table and column names to avoid using reserved words.

Because of that need this annotation processor was born.
Basically it is an annotation processor that validates column names and table names on JPA entities
and fails the compilation if any DBMS reserved words are used (but there is an option to warn only).


## How to use ##

Just add the following dependency maven to your project:
    
    <dependency>
        <groupId>com.flowkode</groupId>
        <artifactId>jpa-reserved-validator</artifactId>
        <version>2.0</version>
        <scope>provided</scope>
    </dependency>

The scope is provided because it is only needed during compilation.

## Options ##
The following options are available:

- **com.flowkode.jpaReservedDetector.doNotUnquote** - If set to true if the table or column names are quoted they will be unquoted,
default is true
- **com.flowkode.jpaReservedDetector.dbms** - Comma separated of the dbms to check for reserved words,
default is the full list "h2, hsqldb, mssql, mysql, oracle, postgres, sqlite"
- **com.flowkode.jpaReservedDetector.failBuild** - If the messages should be Errors or Warnings, 
default is error which will cause the compilation to fail
- **com.flowkode.jpaReservedDetector.extraWords** - Comma separated list of additional words that will be used when checking, default is empty 

Example to show warnings instead of failing the build:

    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
            <compilerArgs>
                <arg>-Acom.flowkode.jpaReservedDetector.failBuild=false</arg>
            </compilerArgs>
            <showWarnings>true</showWarnings>
        </configuration>
    </plugin>

## License ##
This project is licensed under the Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0.txt
