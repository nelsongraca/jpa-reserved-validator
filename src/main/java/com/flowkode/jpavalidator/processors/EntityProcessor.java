package com.flowkode.jpavalidator.processors;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.persistence.*;
import javax.tools.Diagnostic;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;


@SupportedAnnotationTypes({"javax.persistence.Entity", "javax.persistence.MappedSuperclass", "javax.persistence.Embeddable"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedOptions({EntityProcessor.DO_NOT_UNQUOTE, EntityProcessor.DBMS_LIST, EntityProcessor.EXTRA_WORDS, EntityProcessor.FAIL_BUILD})
public class EntityProcessor extends AbstractProcessor {

    //config constants
    public static final String DO_NOT_UNQUOTE = "com.flowkode.jpaReservedDetector.doNotUnquote";

    public static final String DBMS_LIST = "com.flowkode.jpaReservedDetector.dbms";

    public static final String EXTRA_WORDS = "com.flowkode.jpaReservedDetector.extraWords";

    public static final String FAIL_BUILD = "com.flowkode.jpaReservedDetector.failBuild";

    //other constants
    private final String BASE_PATH = "com/flowkode/jpavalidator/words/";

    private final String DBMS_WORDS_EXT = ".txt";

    private final String[] DEFAULT_DBMS_LIST = {"h2", "hsqldb", "mssql", "mysql", "oracle", "postgres", "sqlite"};

    //configs
    private boolean unquote = true;

    private Diagnostic.Kind errorType = Diagnostic.Kind.ERROR;

    //other data
    private Map<String, Map<String, String>> classFields;

    private Map<String, Set<String>> transientClassFields;

    private Map<String, String> classTableNames;

    private Map<String, Map<String, String>> joinTableNames;

    private Map<String, Map<String, Set<String>>> joinTableColumns;

    private Map<String, Set<String>> wordsToCheck = new TreeMap<>(COMPARATOR);

    //todo: maybe in the future add a flag to make it case sensitive/insensitve?
    //custom comparator because we want to be case insensitive
    private static final Comparator<String> COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            if (o1.equalsIgnoreCase(o2)) {
                return 0;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
        }
    };


    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        if (processingEnv.getOptions().get(DO_NOT_UNQUOTE) != null && processingEnv.getOptions().get(DO_NOT_UNQUOTE).equalsIgnoreCase("true")) {
            unquote = false;
        }

        if (processingEnv.getOptions().get(FAIL_BUILD) != null && processingEnv.getOptions().get(FAIL_BUILD).equalsIgnoreCase("false")) {
            errorType = Diagnostic.Kind.WARNING;
        }

        String[] dbmsToUse;
        if (processingEnv.getOptions().get(DBMS_LIST) != null) {
            dbmsToUse = processingEnv.getOptions().get(DBMS_LIST).split(",");
        }
        else {
            dbmsToUse = DEFAULT_DBMS_LIST;
        }

        if (processingEnv.getOptions().get(EXTRA_WORDS) != null) {
            Set<String> words = new TreeSet<>(COMPARATOR);
            for (String word : processingEnv.getOptions().get(EXTRA_WORDS).split(",")) {
                words.add(word.trim());
            }
            wordsToCheck.put("extraWords", words);
        }

        //load all the words
        for (String file : dbmsToUse) {
            InputStream is = getClass().getClassLoader().getResourceAsStream(BASE_PATH + file.trim() + DBMS_WORDS_EXT);

            if (is != null) {
                try (InputStreamReader inputStreamReader = new InputStreamReader(is); BufferedReader br = new BufferedReader(inputStreamReader)) {
                    String line;
                    Set<String> dbmsWords = new TreeSet<>(COMPARATOR);
                    while ((line = br.readLine()) != null) {
                        dbmsWords.add(line);
                    }
                    wordsToCheck.put(file, dbmsWords);
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        //cleanup
        classFields = new TreeMap<>(COMPARATOR);
        transientClassFields = new TreeMap<>(COMPARATOR);
        classTableNames = new TreeMap<>(COMPARATOR);
        joinTableNames = new TreeMap<>(COMPARATOR);
        joinTableColumns = new TreeMap<>(COMPARATOR);

        for (TypeElement annotation : annotations) {
            for (Element element : roundEnv.getElementsAnnotatedWith(annotation)) {
                final String className = ((TypeElement) element).getQualifiedName().toString();
                //add the table name
                final String tableName = getTableName(element);
                if (tableName != null) {
                    classTableNames.put(className, unquote(tableName));
                }

                for (Element enclosedElement : element.getEnclosedElements()) {
                    final String elementName = enclosedElement.getSimpleName().toString();
                    if (enclosedElement.getKind() == ElementKind.FIELD || (enclosedElement.getKind() == ElementKind.METHOD && isGetterOrSetter(elementName))) {
                        addElement(className, enclosedElement);
                    }
                }
            }
        }
        removeTransientFields();

        checkTableNames();
        checkJoinTableNames();
        checkFieldNames();
        checkJoinTableColumns();

        return false;
    }

    private void checkTableNames() {
        for (Map.Entry<String, String> entry : classTableNames.entrySet()) {
            String dbmsWhereIsReservedWord = isReservedWord(entry.getValue());
            if (dbmsWhereIsReservedWord != null) {
                processingEnv.getMessager().printMessage(errorType, String.format("Class '%s' table name '%s' is a reserved word in %s", entry.getKey(), entry.getValue(), dbmsWhereIsReservedWord));
            }
        }
    }

    private void checkJoinTableNames() {
        for (Map.Entry<String, Map<String, String>> entry : joinTableNames.entrySet()) {
            for (Map.Entry<String, String> tableName : entry.getValue().entrySet()) {
                String dbmsWhereIsReservedWord = isReservedWord(tableName.getValue());
                if (dbmsWhereIsReservedWord != null) {
                    processingEnv.getMessager().printMessage(errorType, String.format("Class '%s' field join table '%s' is a reserved word in %s", entry.getKey(), tableName.getValue(), dbmsWhereIsReservedWord));
                }
            }
        }
    }

    private void checkJoinTableColumns() {
        for (Map.Entry<String, Map<String, Set<String>>> entry : joinTableColumns.entrySet()) {
            for (Map.Entry<String, Set<String>> propertyColumns : entry.getValue().entrySet()) {
                for (String columnName : propertyColumns.getValue()) {
                    String dbmsWhereIsReservedWord = isReservedWord(columnName);
                    if (dbmsWhereIsReservedWord != null) {
                        processingEnv.getMessager().printMessage(errorType, String.format("Class '%s' field join column '%s' is a reserved word in %s", entry.getKey(), columnName, dbmsWhereIsReservedWord));
                    }
                }
            }
        }
    }

    private void checkFieldNames() {
        for (Map.Entry<String, Map<String, String>> entry : classFields.entrySet()) {
            for (Map.Entry<String, String> fieldName : entry.getValue().entrySet()) {
                String dbmsWhereIsReservedWord = isReservedWord(fieldName.getValue());
                if (dbmsWhereIsReservedWord != null) {
                    processingEnv.getMessager().printMessage(errorType, String.format("Class '%s' field '%s' is a reserved word in %s", entry.getKey(), fieldName.getValue(), dbmsWhereIsReservedWord));
                }
            }
        }
    }

    private String isReservedWord(String fieldName) {
        //uppercase the field name since in the files they already are
        fieldName = fieldName.toUpperCase();

        //check them all, slower but we can accurately say which dbms uses the word
        StringBuilder foundDbms = new StringBuilder();

        boolean first = true;
        for (Map.Entry<String, Set<String>> entry : wordsToCheck.entrySet()) {
            if (entry.getValue().contains(fieldName)) {
                if (first) {
                    first = false;
                }
                else {
                    foundDbms.append(",");
                }
                foundDbms.append(entry.getKey());
            }
        }

        return foundDbms.toString().isEmpty() ? null : foundDbms.toString();
    }

    private String getTableName(Element element) {
        if (element.getAnnotation(Entity.class) != null) {
            if (element.getAnnotation(Table.class) != null) {
                return element.getAnnotation(Table.class).name();
            }
            return element.getSimpleName().toString();
        }
        return null;
    }

    private boolean isGetterOrSetter(String elementName) {
        return (elementName.startsWith("get") || elementName.startsWith("set")) && elementName.length() > 3;
    }

    private void removeTransientFields() {
        for (Map.Entry<String, Map<String, String>> classFieldsEntry : classFields.entrySet()) {
            for (Iterator<Map.Entry<String, String>> iterator = classFieldsEntry.getValue().entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<String, String> classFields = iterator.next();
                final Set<String> transientFieldSet = transientClassFields.get(classFieldsEntry.getKey());
                if (transientFieldSet != null) {
                    if (transientFieldSet.contains(classFields.getKey())) {
                        iterator.remove();
                    }
                }
            }

        }
    }

    private void addElement(String className, Element enclosedElement) {
        String propertyName;
        String fieldName;

        if (enclosedElement.getKind() == ElementKind.METHOD) {
            propertyName = normalizeGetterSetterName(enclosedElement.getSimpleName().toString());
            fieldName = propertyName;
        }
        else {
            propertyName = enclosedElement.getSimpleName().toString();
            fieldName = propertyName;
        }

        if (enclosedElement.getAnnotation(Column.class) != null) {
            fieldName = enclosedElement.getAnnotation(Column.class).name();
        }
        else if (enclosedElement.getAnnotation(JoinColumn.class) != null) {
            fieldName = enclosedElement.getAnnotation(JoinColumn.class).name();
        }

        fieldName = unquote(fieldName);

        addField(className, propertyName, fieldName);

        //transients are ignored, so are OneToMany, and ManyToMany
        if (enclosedElement.getAnnotation(Transient.class) != null || enclosedElement.getAnnotation(OneToMany.class) != null || enclosedElement.getAnnotation(ManyToMany.class) != null) {
            addTransientField(className, propertyName);

            if (enclosedElement.getAnnotation(JoinTable.class) != null) {
                addJoinTableName(className, propertyName, enclosedElement.getAnnotation(JoinTable.class).name());
                Set<String> columnNames = new HashSet<>();
                for (JoinColumn joinColumn : enclosedElement.getAnnotation(JoinTable.class).joinColumns()) {
                    columnNames.add(joinColumn.name());
                }
                for (JoinColumn joinColumn : enclosedElement.getAnnotation(JoinTable.class).inverseJoinColumns()) {
                    columnNames.add(joinColumn.name());
                }
                addJoinTableColumn(className, propertyName, columnNames);
            }
        }
    }

    private String unquote(String fieldName) {
        if (unquote) {
            if (fieldName.startsWith("\"") && fieldName.endsWith("\"")) {
                fieldName = fieldName.substring(1, fieldName.length() - 1);
            }
        }
        return fieldName;
    }

    private String normalizeGetterSetterName(String elementName) {
        return elementName.substring(3, 4).toLowerCase() + elementName.substring(4);
    }

    private void addField(String className, String propertyName, String fieldName) {
        Map<String, String> fields = classFields.get(className);
        if (fields == null) {
            fields = new TreeMap<>(COMPARATOR);
            classFields.put(className, fields);
        }
        String existingFieldName = fields.get(propertyName);
        if (existingFieldName == null || (!propertyName.equals(fieldName) && !existingFieldName.equals(fieldName))) {
            fields.put(propertyName, fieldName);
        }
    }

    private void addJoinTableColumn(String className, String propertyName, Set<String> columnNames) {

        Map<String, Set<String>> fields = joinTableColumns.get(className);
        if (fields == null) {
            fields = new TreeMap<>(COMPARATOR);
            joinTableColumns.put(className, fields);
        }

        Set<String> existingColumnNames = fields.get(propertyName);
        if (existingColumnNames == null) {
            existingColumnNames = new TreeSet<>(COMPARATOR);
            fields.put(propertyName, existingColumnNames);
        }
        existingColumnNames.addAll(columnNames);
    }

    private void addJoinTableName(String className, String propertyName, String tableName) {
        Map<String, String> fields = joinTableNames.get(className);
        if (fields == null) {
            fields = new TreeMap<>(COMPARATOR);
            joinTableNames.put(className, fields);
        }
        String existingFieldName = fields.get(propertyName);
        if (existingFieldName == null || (!propertyName.equals(tableName) && !existingFieldName.equals(tableName))) {
            fields.put(propertyName, tableName);
        }
    }

    private void addTransientField(String className, String propertyName) {
        Set<String> fields = transientClassFields.get(className);
        if (fields == null) {
            fields = new TreeSet<>(COMPARATOR);
            transientClassFields.put(className, fields);
        }
        fields.add(propertyName);
    }
}
