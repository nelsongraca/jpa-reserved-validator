package com.flowkode.jpavalidator;

import com.github.aliakhtar.annoTest.util.AnnoTest;
import com.github.aliakhtar.annoTest.util.SourceFile;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import com.flowkode.jpavalidator.processors.EntityProcessor;

import javax.persistence.Entity;
import javax.tools.Diagnostic;

import static org.junit.Assert.assertTrue;

public class V13Test extends AnnoTest<EntityProcessor> {
    public V13Test() throws Exception {
        super(new EntityProcessor(), Entity.class);
    }

    @Before
    public void before() {
        //make sure parameters are cleared before each test
        compiler.clearParameterMap();
    }

    @Test
    public void oneToMany() throws Exception {
        SourceFile one = new SourceFile(
                "One.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.Entity;\n" +
                        "import javax.persistence.OneToMany;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class One {\n" +
                        "    @OneToMany(mappedBy = \"one\")\n" +
                        "    private Set<Many> select;\n" +
                        "}"
        );

        SourceFile many = new SourceFile(
                "Many.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.io.Serializable;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class Many {\n" +
                        "    @ManyToOne\n" +
                        "    private One select;\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, one, many));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.v13test.Many' field 'select' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void oneToManyExtra() throws Exception {
        SourceFile one = new SourceFile(
                "One.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.Entity;\n" +
                        "import javax.persistence.OneToMany;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class One {\n" +
                        "    @OneToMany(mappedBy = \"one\")\n" +
                        "    private Set<Many> select;\n" +
                        "}"
        );

        SourceFile many = new SourceFile(
                "Many.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.io.Serializable;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class Many {\n" +
                        "    @ManyToOne\n" +
                        "    @JoinColumn(name = \"select\")\n" +
                        "    private One noConflict;\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, one, many));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.v13test.Many' field 'select' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void manyToMany() throws Exception {
        SourceFile many1 = new SourceFile(
                "ManyOne.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class ManyOne {\n" +
                        "    @ManyToMany" +
                        "    private Set<ManyTwo> select;\n" +
                        "}"
        );

        SourceFile many2 = new SourceFile(
                "ManyTwo.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class ManyTwo {\n" +
                        "    @ManyToMany\n" +
                        "    private Set<ManyOne> select;\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, many1, many2));
        Mockito.verifyNoMoreInteractions(messager);
    }
    @Test
    public void manyToManyExtra() throws Exception {
        SourceFile many1 = new SourceFile(
                "ManyOne.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.Entity;\n" +
                        "import javax.persistence.JoinColumn;\n" +
                        "import javax.persistence.JoinTable;\n" +
                        "import javax.persistence.ManyToMany;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class ManyOne {\n" +
                        "    @ManyToMany\n" +
                        "    @JoinTable(name = \"select\",\n" +
                        "            joinColumns = {@JoinColumn(name = \"from\")},\n" +
                        "            inverseJoinColumns = {@JoinColumn(name = \"where\")})\n" +
                        "    private Set<ManyTwo> okField;\n" +
                        "}"
        );

        SourceFile many2 = new SourceFile(
                "ManyTwo.java",
                "package com.flowkode.jpavalidator.v13test;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.util.Set;\n" +
                        "\n" +
                        "@Entity \n" +
                        "public class ManyTwo {\n" +
                        "    @ManyToMany(mappedBy = \"okField\")\n" +
                        "    private Set<ManyOne> otherField;\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, many1, many2));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.v13test.ManyOne' field join table 'select' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.v13test.ManyOne' field join column 'from' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.v13test.ManyOne' field join column 'where' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }
}
