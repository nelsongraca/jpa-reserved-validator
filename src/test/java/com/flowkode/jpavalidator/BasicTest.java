package com.flowkode.jpavalidator;

import com.flowkode.jpavalidator.processors.EntityProcessor;
import com.github.aliakhtar.annoTest.util.AnnoTest;
import com.github.aliakhtar.annoTest.util.SourceFile;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.Entity;
import javax.tools.Diagnostic;

import static org.junit.Assert.assertTrue;

public class BasicTest extends AnnoTest<EntityProcessor> {
    public BasicTest() throws Exception {
        super(new EntityProcessor(), Entity.class);
    }


    @Before
    public void before() {
        //make sure parameters are cleared before each test
        compiler.clearParameterMap();
    }

    @Test
    public void oracleWithWarnings() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.Column;\n" +
                        "import javax.persistence.Entity;\n" +
                        "import javax.persistence.Transient;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    //all of them are oracle reerved words, should warn about 3 of them, MAXSIZE,label and file\n" +
                        "    \n" +
                        "    @Column(name = \"MAXSIZE\")\n" +
                        "    private String fieldOne;\n" +
                        "    \n" +
                        "    private String label;\n" +
                        "    \n" +
                        "    @Transient\n" +
                        "    private String select;\n" +
                        "    \n" +
                        "    \n" +
                        "    public void setFile() {\n" +
                        "        \n" +
                        "    }\n" +
                        "    \n" +
                        "    \n" +
                        "}"
        );
        compiler.putParameter(EntityProcessor.DBMS_LIST, "oracle");
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'label' is a reserved word in oracle");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'MAXSIZE' is a reserved word in oracle");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'file' is a reserved word in oracle");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void allDBMSWithError() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "" +
                        "import javax.persistence.Entity; \n" +
                        "\n" +
                        "" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    //these two are reserved in most dbms\n" +
                        "    private String order;\n" +
                        "    \n" +
                        "    private String upper;\n" +
                        "    \n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'order' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'upper' is a reserved word in hsqldb,mssql,postgres");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void unquote() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.Column;\n" +
                        "import javax.persistence.Entity; \n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @Column(name=\"\\\"order\\\"\")\n" +
                        "    private String order;\n" +
                        "    \n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'order' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void noUnquote() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.Column;\n" +
                        "import javax.persistence.Entity; \n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @Column(name=\"\\\"order\\\"\")\n" +
                        "    private String order;\n" +
                        "    \n" +
                        "}"
        );
        compiler.putParameter(EntityProcessor.DO_NOT_UNQUOTE, "true");
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verifyNoMoreInteractions(messager);
    }


    @Test
    public void entityWithReservedName() throws Exception {
        SourceFile badEntity = new SourceFile(
                "ThisIsValid.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "" +
                        "import javax.persistence.Entity;\n" +
                        "import javax.persistence.Table; \n" +
                        "\n" +
                        "" +
                        "//where is reserved\n" +
                        "@Entity\n" +
                        "@Table(name=\"where\")\n" +
                        "public class ThisIsValid {\n" +
                        "\n" +
                        "    \n" +
                        "}"
        );
        SourceFile anotherEntity = new SourceFile(
                "Order.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "" +
                        "import javax.persistence.Entity; \n" +
                        "\n" +
                        "" +
                        "//order is reserved\n" +
                        "@Entity\n" +
                        "\n" +
                        "public class Order {\n" +
                        "\n" +
                        "    \n" +
                        "}"
        );

        assertTrue(compiler.compileWithProcessor(processor, badEntity, anotherEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.Order' table name 'Order' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.ThisIsValid' table name 'where' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void capitalizationBug() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.Column;\n" +
                        "import javax.persistence.Entity; \n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @Column(name=\"\\\"ORDER\\\"\")\n" +
                        "    private String order;\n" +
                        "\n" +
                        "    public String getOrder() {\n" +
                        "        return order;\n" +
                        "    }\n" +
                        "\n" +
                        "    public void setOrder(String order) {\n" +
                        "        this.order = order;\n" +
                        "    }\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'ORDER' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void badDetectionBug() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.*; \n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @ManyToOne(fetch = FetchType.LAZY)\n" +
                        "    @JoinColumn(name = \"ORGANIZATION_ID\")\n" +
                        "    private String organization;\n" +
                        "\n" +
                        "    public String getOrganization() {\n" +
                        "        return organization;\n" +
                        "    }\n" +
                        "\n" +
                        "    public void setOrganization(String organization) {\n" +
                        "        this.organization = organization;\n" +
                        "    }\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verifyNoMoreInteractions(messager);
    }

    @Test
    public void mappedSuperClass() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.*; \n" +
                        "\n" +
                        "@MappedSuperclass\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @Column(name = \"select\")\n" +
                        "    private String stuff;\n" +
                        "\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'select' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }


    @Test
    public void embeddableClass() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.io.Serializable;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "\n" +
                        "    @Embeddable\n" +
                        "    public static class SubEntity implements Serializable {\n" +
                        "\n" +
                        "        private String select;\n" +
                        "\n" +
                        "   \n" +
                        "    }\n" +
                        "}"
        );
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity.SubEntity' field 'select' is a reserved word in h2,hsqldb,mssql,mysql,oracle,postgres,sqlite");
        Mockito.verifyNoMoreInteractions(messager);
    }
    @Test
    public void extraWords() throws Exception {
        SourceFile badEntity = new SourceFile(
                "OneEntity.java",
                "package com.flowkode.jpavalidator.basictest;\n" +
                        "\n" +
                        "import javax.persistence.*;\n" +
                        "import java.io.Serializable;\n" +
                        "\n" +
                        "@Entity\n" +
                        "public class OneEntity {\n" +
                        "    private String status;\n" +
                        "    \n" +
                        "    private String anotherWord;\n" +
                        "}"
        );
        compiler.putParameter(EntityProcessor.EXTRA_WORDS, "status,anotherWord");
        assertTrue(compiler.compileWithProcessor(processor, badEntity));
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'anotherWord' is a reserved word in extraWords");
        Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR, "Class 'com.flowkode.jpavalidator.basictest.OneEntity' field 'status' is a reserved word in extraWords");
        Mockito.verifyNoMoreInteractions(messager);
    }
}
